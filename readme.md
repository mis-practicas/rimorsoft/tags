## Sistema de Etiquetas (TAGs)

Cuando comprendas la importancia de este tema te estudiarás este curso al menos cuatro (4) veces. Vemos este tipo de sistemas en gmail, trello y muchas herramientas similares.

Este tema es vital si queremos que los datos de nuestro proyecto web puedan ser exageradamente organizados.

[Rimorsoft - Curso](https://rimorsoft.com/serie/sistema-de-etiquetas-tags)

## Paquetes instalados

- [Laravel Tagging](https://github.com/rtconner/laravel-tagging).
